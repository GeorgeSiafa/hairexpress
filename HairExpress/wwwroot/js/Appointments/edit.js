﻿
$('.timepicker').timepicker({
    timeFormat: "HH:mm",
    interval: "15",
    minTime: "10:00",
    maxTime: "18:00",
    use24hours: true,
    dropdown: true,
    scrollbar: true
});

        $(function () {
        $('#datepicker').datepicker({
            minDate: +1, maxDate: "+3M",
            dateFormat: 'dd-mm-yy'

        });
    });

        $(document).ready(function () {
            var appointmentTime = document.getElementById('datepicker').value;
    var splitData = "";
            if (appointmentTime.indexOf(" ") > 0) {
        splitData = appointmentTime.split(" ");

    }
            else {
        splitData = appointmentTime.split("T");

    }

    var time = splitData[1].substring(0, 5);
            if (time.slice(-1) == ":") {
        time = time.substring(0, time.length - 1);

    }
    var amPmTime = splitData[2];
    $('#datepicker').attr('value', splitData[0]);
    $('#timepicker').attr('value', time + ' ' + amPmTime);

});

var ProfilesEdit = function () {

   

    return {
        init: function (customConfig) {
            config = $.extend(true, {}, config, customConfig);

            initialiseForm();
            handleForm();
            handlePrinterTrays();
        }
    };
}();
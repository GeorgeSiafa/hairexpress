﻿using HairExpress.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace HairExpress.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<HairType> HairTypes { get; set; }

        public DbSet<SpecialOffer> SpecialOffers { get; set; }
        public DbSet<Product> Products { get; set; }

        public DbSet<Appointments> Appointments { get; set; }
        public DbSet<ProductSelectedForAppointment> ProductSelectedForAppointments { get; set; }
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
    }
}

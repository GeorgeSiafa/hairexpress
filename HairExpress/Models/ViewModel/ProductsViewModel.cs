﻿using System.Collections.Generic;

namespace HairExpress.Models.ViewModel
{
    public class ProductsViewModel
    {
        public Product Products { get; set; }
        public IEnumerable<HairType> HairTypes { get; set; }
        public IEnumerable<SpecialOffer> SpecialOffers { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;


namespace HairExpress.Models.ViewModel
{
    public class ShoppingCartViewModel
    {
        public List<Product> Products { get; set; }
        public Appointments Appointments { get; set; }
    }
}

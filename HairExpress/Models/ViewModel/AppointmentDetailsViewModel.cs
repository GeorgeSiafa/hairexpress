﻿using System.Collections.Generic;


namespace HairExpress.Models.ViewModel
{
    public class AppointmentDetailsViewModel
    {
        public Appointments Appointments { get; set; }
        public List<ApplicationUser> HairDresser { get; set; }
        public List<Product> Products { get; set; }
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace HairExpress.Models
{
    public class Product
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public bool Available { get; set; }

        public string Image { get; set; }

        public string ShadeColor { get; set; }

        [Display(Name = "Hair Type")]
        public Guid HairTypeId { get; set; }

        [ForeignKey("HairTypeId")]
        public virtual HairType HairTypes { get; set; }

        [Display(Name = "Speical Offer")]
        public Guid SpecialOfferId { get; set; }

        [ForeignKey("SpecialOfferId")]
        public virtual SpecialOffer SpecialOffers { get; set; }
    }
}

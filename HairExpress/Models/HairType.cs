﻿using System;
using System.ComponentModel.DataAnnotations;


namespace HairExpress.Models
{
    public class HairType
    {
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations.Schema;


namespace HairExpress.Models
{
    public class ProductSelectedForAppointment
    {
        public Guid Id { get; set; }

        public Guid AppointmentId { get; set; }

        [ForeignKey("AppointmentId")]
        public virtual Appointments Appointments { get; set; }

        public Guid ProductId { get; set; }

        [ForeignKey("ProductId")]
        public virtual Product Products { get; set; }
    }
}

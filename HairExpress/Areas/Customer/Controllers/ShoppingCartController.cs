﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HairExpress.Data;
using HairExpress.Extensions;
using HairExpress.Models;
using HairExpress.Models.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace HairExpress.Areas.Customer.Controllers
{
    [Area("Customer")]
    public class ShoppingCartController : Controller
    {
        private readonly ApplicationDbContext _db;

        [BindProperty]
        public ShoppingCartViewModel ShoppingCartVM { get; set; }

        public ShoppingCartController(ApplicationDbContext db)
        {
            _db = db;
            ShoppingCartVM = new ShoppingCartViewModel()
            {
                Products = new List<Product>()
            };
        }
        //Get Index Shopping Cart
        public async Task<IActionResult> Index()
        {
            List<Guid> lstShoppingCart = HttpContext.Session.Get<List<Guid>>("ssShoppingCart");
            if (lstShoppingCart.Count > 0)
            {
                foreach (Guid cartItem in lstShoppingCart)
                {
                    Product prod = _db.Products.Include(p => p.SpecialOffers).Include(p => p.HairTypes).Where(p => p.Id == cartItem).FirstOrDefault();
                    ShoppingCartVM.Products.Add(prod);
                }
            }
            return View(ShoppingCartVM);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Index")]
        public IActionResult IndexPost()
        {
            List<Guid> lstCartItems = HttpContext.Session.Get<List<Guid>>("ssShoppingCart");

            ShoppingCartVM.Appointments.AppointmentDate = ShoppingCartVM.Appointments.AppointmentDate
                                                            .AddHours(ShoppingCartVM.Appointments.AppointmentTime.Hour)
                                                            .AddMinutes(ShoppingCartVM.Appointments.AppointmentTime.Minute);

            Appointments appointments = ShoppingCartVM.Appointments;
            _db.Appointments.Add(appointments);
            _db.SaveChanges();

            Guid appointmentId = appointments.Id;

            foreach (Guid productId in lstCartItems)
            {
                ProductSelectedForAppointment productSelectedForAppointments = new ProductSelectedForAppointment()
                {
                    AppointmentId = appointmentId,
                    ProductId = productId
                };
                _db.ProductSelectedForAppointments.Add(productSelectedForAppointments);

            }
            _db.SaveChanges();
            lstCartItems = new List<Guid>();
            HttpContext.Session.Set("ssShoppingCart", lstCartItems);

            return RedirectToAction("AppointmentConfirmation", "ShoppingCart", new { Id = appointmentId });

        }

        public IActionResult Remove(Guid id)
        {
            List<Guid> lstCartItems = HttpContext.Session.Get<List<Guid>>("ssShoppingCart");

            if (lstCartItems.Count > 0)
            {
                if (lstCartItems.Contains(id))
                {
                    lstCartItems.Remove(id);
                }
            }

            HttpContext.Session.Set("ssShoppingCart", lstCartItems);

            return RedirectToAction(nameof(Index));
        }


        //Get
        public IActionResult AppointmentConfirmation(Guid id)
        {
            ShoppingCartVM.Appointments = _db.Appointments.Where(a => a.Id == id).FirstOrDefault();
            List<ProductSelectedForAppointment> objProdList = _db.ProductSelectedForAppointments.Where(p => p.AppointmentId == id).ToList();

            foreach (ProductSelectedForAppointment prodAptObj in objProdList)
            {
                ShoppingCartVM.Products.Add(_db.Products.Include(p => p.HairTypes).Include(p => p.SpecialOffers).Where(p => p.Id == prodAptObj.ProductId).FirstOrDefault());
            }

            return View(ShoppingCartVM);
        }

    }
}
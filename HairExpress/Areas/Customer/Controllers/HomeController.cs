﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using HairExpress.Data;
using HairExpress.Extensions;
using HairExpress.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace HairExpress.Areas.Customer.Controllers
{
    [Area("Customer")]
    public class HomeController : Controller
    {

        private readonly ApplicationDbContext _db;

        public HomeController(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task<IActionResult> Index()
        {
            var productList = await _db.Products.Include(m => m.HairTypes).Include(m => m.SpecialOffers).ToListAsync();

            return View(productList);
        }

        public async Task<IActionResult> Details(Guid id)
        {
            var product = await _db.Products.Include(m => m.HairTypes).Include(m => m.SpecialOffers).Where(m => m.Id == id).FirstOrDefaultAsync();


            return View(product);
        }

        [HttpPost, ActionName("Details")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DetailsPost(Guid id)
        {
            List<Guid> lstShoppingCart = HttpContext.Session.Get<List<Guid>>("ssShoppingCart");
            if (lstShoppingCart == null)
            {
                lstShoppingCart = new List<Guid>();
            }
            lstShoppingCart.Add(id);
            HttpContext.Session.Set("ssShoppingCart", lstShoppingCart);

            return RedirectToAction("Index", "Home", new { area = "Customer" });

        }

        public IActionResult Remove(Guid id)
        {
            List<Guid> lstShoppingCart = HttpContext.Session.Get<List<Guid>>("ssShoppingCart");
            if (lstShoppingCart.Count > 0)
            {
                if (lstShoppingCart.Contains(id))
                {
                    lstShoppingCart.Remove(id);
                }
            }

            HttpContext.Session.Set("ssShoppingCart", lstShoppingCart);

            return RedirectToAction(nameof(Index));
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HairExpress.Data;
using HairExpress.Models;
using Microsoft.AspNetCore.Mvc;

namespace HairExpress.Areas.Admin.Controllers
{
    //[Authorize(Roles = SD.SuperAdminEndUser)]
    [Area("Admin")]
    public class HairTypesController : Controller
    {

        private readonly ApplicationDbContext _db;

        public HairTypesController(ApplicationDbContext db)
        {
            _db = db;
        }

        public IActionResult Index()
        {
            return View(_db.HairTypes.ToList());
        }
        //GET Create Action Method
        public IActionResult Create()
        {
            return View();
        }

        //POST Create action Method
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(HairType hairTypes)
        {
            if (ModelState.IsValid)
            {
                _db.Add(hairTypes);
                await _db.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(hairTypes);
        }


        //GET Edit Action Method
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hairType = await _db.HairTypes.FindAsync(id);
            if (hairType == null)
            {
                return NotFound();
            }

            return View(hairType);
        }

        //POST Edit action Method
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, HairType hairType)
        {
            if (id != hairType.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                _db.Update(hairType);
                await _db.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(hairType);
        }

        //GET Details Action Method
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hairType = await _db.HairTypes.FindAsync(id);
            if (hairType == null)
            {
                return NotFound();
            }

            return View(hairType);
        }


        //GET Delete Action Method
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hairType = await _db.HairTypes.FindAsync(id);
            if (hairType == null)
            {
                return NotFound();
            }

            return View(hairType);
        }

        //POST Delete action Method
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var productTypes = await _db.HairTypes.FindAsync(id);
            _db.HairTypes.Remove(productTypes);
            await _db.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

    }
}